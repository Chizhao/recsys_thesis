## Performance of Each Model on MovieLens 100k 

| Model | Basic NeuralCF                 | Basic TwoTowerNeuralCF                 |
|-------|--------------------------------|----------------------------------------|
| Rmse  | 0.961                          | **0.935**                              |
| Epoch | 96                             | **17**                                 |
| Time  | 509.307 seconds                | **136.022 seconds**                    |
| Model | NeuralCF with Fixed Features   | TwoTowerNeuralCF with Fixed Features   |
| Rmse  | **0.916**                      | 0.922                                  |
| Epoch | **9**                          | 11                                     |
| Time  | 139.816 seconds                | **139.585 seconds**                    |
| Model | NeuralCF with Dynamic Features | TwoTowerNeuralCF with Dynamic Features |
| Rmse  | **0.926**                      | 0.938                                  |
| Epoch | **11**                         | 28                                     |
| Time  | **168.074 seconds**            | 337.398 seconds                        |





## Performance of Each Model on MovieLens 1m 

| Model | Basic NeuralCF                 | Basic TwoTowerNeuralCF                 |
|-------|--------------------------------|----------------------------------------|
| Rmse  | 1.040                          | **0.873**                              |
| Epoch | **30**                         | 66                                     |
| Time  | **2710.935 seconds**           | 5321.963 seconds                       |
| Model | NeuralCF with Fixed Features   | TwoTowerNeuralCF with Fixed Features   |
| Rmse  | **0.857**                      | 0.869                                  |
| Epoch | **17**                         | 73                                     |
| Time  | **2137.132 seconds**           | 6401.321 seconds                       |
| Model | NeuralCF with Dynamic Features | TwoTowerNeuralCF with Dynamic Features |
| Rmse  | **0.867**                      | 0.868                                  |
| Epoch | **31**                         | 57                                     |
| Time  | **3555.280 seconds**           | 5609.648 seconds                       |
