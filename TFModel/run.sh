#usage: NeuralCF.py [-h] [--fname {ml-1m,ml-100k}] [--onlyUseId {0,1}]
#                   [--useBasicModel {0,1}] [--epochs EPOCHS]
#                   [--useNeuralCF USENEURALCF] [--onlySummary {0,1}] [--run]
#
#optional arguments:
#  -h, --help            show this help message and exit
#  --fname {ml-1m,ml-100k}
#                        choose the dataset
#  --onlyUseId {0,1}     onlyUseId or not
#  --useBasicModel {0,1}
#                        useBasicModel or not
#  --epochs EPOCHS       set the epochs for your model
#  --useNeuralCF USENEURALCF
#                        choose model
#  --onlySummary {0,1}   onlySummary or not
#  --run                 start training model


# --useBasicModel = 1 && --onlyUseId = 1 && --useNeuralCF = 1 -> basicNeuralCF *
# --useBasicModel = 1 && --onlyUseId = 1 && --useNeuralCF = 0 -> basicTwoTowerNeuralCF *
# --useBasicModel = 1 && --onlyUseId = 0 && --useNeuralCF = 1 -> NeuralCF-FixedFeatures
# --useBasicModel = 1 && --onlyUseId = 0 && --useNeuralCF = 0 -> TwoTowerNeuralCF-FixedFeatures *
# --useBasicModel = 0 && --onlyUseId = 0 && --useNeuralCF = 1 -> NeuralCF-DynamicFeatures
# --useBasicModel = 0 && --onlyUseId = 0 && --useNeuralCF = 0 -> TwoTowerNeuralCF-DynamicFeatures

# MovieLens 100k
## 1. MovieLens 100k - basicNeuralCF Model 111
fname='ml-100k'
modelName='basicNeuralCF'
time=$(date '+%Y%m%d-%H%M')
output=./runningLogs/${fname}-${modelName}-${time}.log
python NeuralCF.py  --fname ml-100k --epochs 100 --useBasicModel 1 --onlyUseId 1 --useNeuralCF 1 --run > ${output}

## 2. MovieLens 100k - basicTwoTowerNeuralCF 110
fname='ml-100k'
modelName='basicTwoTowerNeuralCF'
time=$(date '+%Y%m%d-%H%M')
output=./runningLogs/${fname}-${modelName}-${time}.log
python NeuralCF.py  --fname ml-100k --epochs 100 --useBasicModel 1 --onlyUseId 1 --useNeuralCF 0 --run > ${output}

## 3. MovieLens 100k - NeuralCF-FixedFeatures 101
fname='ml-100k'
modelName='NeuralCF-FixedFeatures'
time=$(date '+%Y%m%d-%H%M')
output=./runningLogs/${fname}-${modelName}-${time}.log
python NeuralCF.py  --fname ml-100k --epochs 100 --useBasicModel 1 --onlyUseId 0 --useNeuralCF 1 --run > ${output}

## 4. MovieLens 100k - TwoTowerNeuralCF-FixedFeatures 100
fname='ml-100k'
modelName='TwoTowerNeuralCF-FixedFeatures'
time=$(date '+%Y%m%d-%H%M')
output=./runningLogs/${fname}-${modelName}-${time}.log
python NeuralCF.py  --fname ml-100k --epochs 100 --useBasicModel 1 --onlyUseId 0 --useNeuralCF 0 --run > ${output}

## 5. MovieLens 100k - NeuralCF-DynamicFeatures 001
fname='ml-100k'
modelName='NeuralCF-DynamicFeatures'
time=$(date '+%Y%m%d-%H%M')
output=./runningLogs/${fname}-${modelName}-${time}.log
python NeuralCF.py  --fname ml-100k --epochs 100 --useBasicModel 0 --onlyUseId 0 --useNeuralCF 1 --run > ${output}

## 6. MovieLens 100k - TwoTowerNeuralCF-DynamicFeatures 000
fname='ml-100k'
modelName='TwoTowerNeuralCF-DynamicFeatures'
time=$(date '+%Y%m%d-%H%M')
output=./runningLogs/${fname}-${modelName}-${time}.log
python NeuralCF.py  --fname ml-100k --epochs 100 --useBasicModel 0 --onlyUseId 0 --useNeuralCF 0 --run > ${output}


# MovieLens 1m
## 1. MovieLens 1m - basicNeuralCF Model 111
fname='ml-1m'
modelName='basicNeuralCF'
time=$(date '+%Y%m%d-%H%M')
output=./runningLogs/${fname}-${modelName}-${time}.log
python NeuralCF.py  --fname ml-1m --epochs 100 --useBasicModel 1 --onlyUseId 1 --useNeuralCF 1 --run > ${output}

## 2. MovieLens 1m - basicTwoTowerNeuralCF 110
fname='ml-1m'
modelName='basicTwoTowerNeuralCF'
time=$(date '+%Y%m%d-%H%M')
output=./runningLogs/${fname}-${modelName}-${time}.log
python NeuralCF.py  --fname ml-1m --epochs 100 --useBasicModel 1 --onlyUseId 1 --useNeuralCF 0 --run > ${output}

## 3. MovieLens 1m - NeuralCF-FixedFeatures 101
fname='ml-1m'
modelName='NeuralCF-FixedFeatures'
time=$(date '+%Y%m%d-%H%M')
output=./runningLogs/${fname}-${modelName}-${time}.log
python NeuralCF.py  --fname ml-1m --epochs 100 --useBasicModel 1 --onlyUseId 0 --useNeuralCF 1 --run > ${output}

## 4. MovieLens 1m - TwoTowerNeuralCF-FixedFeatures 100
fname='ml-1m'
modelName='TwoTowerNeuralCF-FixedFeatures'
time=$(date '+%Y%m%d-%H%M')
output=./runningLogs/${fname}-${modelName}-${time}.log
python NeuralCF.py  --fname ml-1m --epochs 100 --useBasicModel 1 --onlyUseId 0 --useNeuralCF 0 --run > ${output}

## 5. MovieLens 1m - NeuralCF-DynamicFeatures 001
fname='ml-1m'
modelName='NeuralCF-DynamicFeatures'
time=$(date '+%Y%m%d-%H%M')
output=./runningLogs/${fname}-${modelName}-${time}.log
python NeuralCF.py  --fname ml-1m --epochs 100 --useBasicModel 0 --onlyUseId 0 --useNeuralCF 1 --run > ${output}

## 6. MovieLens 1m - TwoTowerNeuralCF-DynamicFeatures 000
fname='ml-1m'
modelName='TwoTowerNeuralCF-DynamicFeatures'
time=$(date '+%Y%m%d-%H%M')
output=./runningLogs/${fname}-${modelName}-${time}.log
python NeuralCF.py  --fname ml-1m --epochs 100 --useBasicModel 0 --onlyUseId 0 --useNeuralCF 0 --run > ${output}

# remove redundant files
find runningLogs/ -type f -name "*.log" -size 0c|xargs -n 1 rm -f