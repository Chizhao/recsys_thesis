#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2/19/21 15:27
# @Author  : zhaochi
# @Email   : dandanv5@hotmail.com
# @File    : NeuralCF.py
# @Software: PyCharm
import argparse
import datetime
import time

import dill as pickle
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras.utils import plot_model

# noinspection PyUnresolvedReferences
from helper import GENRE_VOCAB_100K, GENRE_VOCAB_1M, OCCUPATION_VOCAB_100K, OCCUPATION_VOCAB_1M, AGE_VOCAB, \
    LOCATION_VOCAB, GENDER_VOCAB, INPUTS
# noinspection PyUnresolvedReferences
from helper import URL_PREFIX, CSV100K, CSV1M, AGE_NAME, NUM_BUCKETS
from helper import getTrainTestDataset

# set random seed for reproduce experiment
SEED = 23333
np.random.seed(SEED)
tf.random.set_seed(SEED)
CURRENT_TIME = datetime.datetime.now().strftime("%Y%m%d-%H%M")

tf.get_logger().setLevel('ERROR')

NUMERICAL_FEATURES_NAME = ['movieAvgRating', 'movieRatingStddev', 'movieRatingCount', 'releaseYear',
                           'userReleaseYearStddev', 'userAvgReleaseYear', 'userAvgRating', 'userRatingStddev',
                           'userRatingCount']

CATEGORICAL_FEATURES_NAME = ['movieId', 'userId', 'movieGenre1', 'movieGenre2', 'movieGenre3', 'userGenre1',
                             'userGenre2', 'userGenre3',
                             'userGenre4', 'userGenre5', 'userRatedMovie1', 'userRatedMovie2', 'userRatedMovie3',
                             'userRatedMovie4', 'userRatedMovie5',
                             'age', 'ageGroup', 'gender', 'occupation', 'location']


def get_trainDataSetDF(fname: str) -> pd.DataFrame:
    """

    Args:
        fname: dataset name
    Returns:
        trainDataFrame
    """
    link = URL_PREFIX + CSV100K[0]
    if fname == 'ml-1m':
        link = URL_PREFIX + CSV1M[0]
    return pd.read_csv(link)


def get_normalization_parameters(trainDF: pd.DataFrame, num_feas: list) -> dict:
    """

    Args:
        trainDF: DataFrame of trainData
        num_feas: list of all numerical feature columns

    Returns:
        dict: key - fea, value dict of mean and std
    """

    def _zScore_params(column):
        mean = trainDF[column].mean()
        std = trainDF[column].mean()
        return {'mean': mean, 'std': std}

    normalization_parameters = {}
    for fea in num_feas:
        normalization_parameters[fea] = _zScore_params(fea)
    return normalization_parameters


def generate_features_input(fname):
    """

    Args:
        fname: dataset name ['ml-1m', 'ml-100k']
        isBasicModel: bool

    Returns: movie_features_dict, user_features_dict, user_behaviour_dict

    """
    movie_dynamicFeatures_list = ['movieAvgRating', 'movieRatingStddev', 'movieRatingCount']
    movie_fixFeatures_list = ['movieId', 'releaseYear', 'movieGenre1', 'movieGenre2', 'movieGenre3']
    user_dynamicFeatures_list = ['userRatedMovie1', 'userRatedMovie2', 'userRatedMovie3', 'userRatedMovie4',
                                 'userRatedMovie5', 'userReleaseYearStddev',
                                 'userAvgReleaseYear', 'userAvgRating', 'userRatingStddev', 'userRatingCount',
                                 'userGenre1', 'userGenre2', 'userGenre3', 'userGenre4', 'userGenre5']
    user_fixFeatures_list = ['userId', 'gender', AGE_NAME[fname], 'occupation',
                             'location']  # use ageGroup rather than age
    movie_fixFeatures_dict = {k: v for k, v in INPUTS.items() if k in movie_fixFeatures_list}
    user_fixFeatures_dict = {k: v for k, v in INPUTS.items() if k in user_fixFeatures_list}
    print(user_fixFeatures_dict)
    movie_dynamicFeatures_dict = {k: v for k, v in INPUTS.items() if k in movie_dynamicFeatures_list}
    user_dynamicFeatures_dict = {k: v for k, v in INPUTS.items() if k in user_dynamicFeatures_list}
    return movie_dynamicFeatures_dict, movie_fixFeatures_dict, user_fixFeatures_dict, user_dynamicFeatures_dict


def get_categorical_features_dict(fname):
    suffix = fname.split(sep='-')[1].upper()
    genre_vocab = eval('GENRE_VOCAB_' + suffix)
    occupation_vocab = eval('OCCUPATION_VOCAB_' + suffix)
    userId_list = list(np.arange(NUM_BUCKETS['userId_' + fname]))
    movieId_list = list(np.arange(NUM_BUCKETS['movieId_' + fname]))
    categorical_features_dict = {
        'userId': userId_list,
        'movieId': movieId_list,
        'movieGenre1': genre_vocab,
        'movieGenre2': genre_vocab,
        'movieGenre3': genre_vocab,
        'userGenre1': genre_vocab,
        'userGenre2': genre_vocab,
        'userGenre3': genre_vocab,
        'userGenre4': genre_vocab,
        'userGenre5': genre_vocab,
        'userRatedMovie1': movieId_list,
        'userRatedMovie2': movieId_list,
        'userRatedMovie3': movieId_list,
        'userRatedMovie4': movieId_list,
        'userRatedMovie5': movieId_list,
        AGE_NAME[fname]: AGE_VOCAB,  # add ageGroup
        'gender': GENDER_VOCAB,
        'occupation': occupation_vocab,
        'location': LOCATION_VOCAB
    }
    return categorical_features_dict


def generate_feature_columns(input_dict: dict, categorical_features_dict: dict, normalization_parameters: dict) -> list:
    """
    Args:
        input_dict:
        categorical_features_dict:

    Returns:
        feature_columns: list of feature column
    """
    feature_columns = []
    for feature, _ in input_dict.items():
        if feature in NUMERICAL_FEATURES_NAME:
            # continue
            mean = normalization_parameters[feature]['mean']
            std = normalization_parameters[feature]['std']
            feature_columns.append(
                tf.feature_column.numeric_column(feature, normalizer_fn=lambda col: (col - mean) / std))
        else:
            cat_col = tf.feature_column.categorical_column_with_vocabulary_list(key=feature,
                                                                                vocabulary_list=
                                                                                categorical_features_dict[feature])
            emb_col = tf.feature_column.embedding_column(cat_col, dimension=64)
            feature_columns.append(emb_col)

    return feature_columns


class twoTower_neuralCF_moreFea(object):
    batch_size = 128

    def __init__(self, fname: str, basicModel: bool, onlyUseId: bool):
        """
        Args:
            fname:
            basicModel: 1 two tower NeuralCF model with more feature, 0 two tower NeuralCF model consider dynamic features
        """
        if fname == 'ml-1m':
            self.batch_size = 256
        try:
            INPUTS.pop('age')
            INPUTS.pop('ageGroup')
        except:
            KeyError
        INPUTS[AGE_NAME[fname]] = tf.keras.layers.Input(name=AGE_NAME[fname], shape=(1,), dtype='string')
        if onlyUseId:
            self.feature_inputs = {
                'userId': INPUTS['userId'],
                'movieId': INPUTS['movieId'],
            }
        else:
            self.feature_inputs = INPUTS
        self.fname = fname
        self.basicModel = basicModel
        self.onlyUseId = onlyUseId

    def generate_input_featureColumns(self):
        self.train, self.test = getTrainTestDataset(self.fname)
        self.trainDF = None
        self.normalization_parameters = None
        if self.basicModel:
            try:
                self.feature_inputs.pop('movieAvgRating')
                self.feature_inputs.pop('userAvgRating')
                self.feature_inputs.pop('userGenre1')
                self.feature_inputs.pop('userGenre2')
            except:
                KeyError
        else:
            self.trainDF = get_trainDataSetDF(self.fname)
            self.normalization_parameters = get_normalization_parameters(self.trainDF, NUMERICAL_FEATURES_NAME)

        self.categorical_features_dict = get_categorical_features_dict(self.fname)
        if self.onlyUseId:
            movie_fixFeatures_dict = {'movieId': self.feature_inputs['movieId']}
            user_fixFeatures_dict = {'userId': self.feature_inputs['userId']}
        else:
            movie_dynamicFeatures_dict, movie_fixFeatures_dict, \
            user_fixFeatures_dict, user_dynamicFeatures_dict = generate_features_input(self.fname)

        self.movie_fixFeatures_input = movie_fixFeatures_dict
        self.movie_fixFeatures_columns = generate_feature_columns(self.movie_fixFeatures_input,
                                                                  self.categorical_features_dict,
                                                                  self.normalization_parameters)
        self.user_fixFeatures_input = user_fixFeatures_dict

        self.user_fixFeatures_columns = generate_feature_columns(self.user_fixFeatures_input,
                                                                 self.categorical_features_dict,
                                                                 self.normalization_parameters)
        if not self.basicModel:
            self.movie_dynamicFeatures_input = movie_dynamicFeatures_dict
            self.user_dynamicFeatures_input = user_dynamicFeatures_dict
            self.movie_dynamicFeatures_columns = generate_feature_columns(self.movie_dynamicFeatures_input,
                                                                          self.categorical_features_dict,
                                                                          self.normalization_parameters)
            self.user_dynamicFeatures_columns = generate_feature_columns(self.user_dynamicFeatures_input,
                                                                         self.categorical_features_dict,
                                                                         self.normalization_parameters)

    def build_model(self, hidden_units: list):

        # fix feature tower
        ## movie fix features tower
        movie_fixFeatures_tower = tf.keras.layers.DenseFeatures(self.movie_fixFeatures_columns,
                                                                name='movie_fixFeatures_tower_embedding')(
            self.movie_fixFeatures_input)
        for i, num_nodes in enumerate(hidden_units):
            movie_fixFeatures_tower = tf.keras.layers.Dense(num_nodes, activation='relu',
                                                            name='movie_fixFeatures_tower_MLP' + str(i))(
                movie_fixFeatures_tower)
        movie_fixFeatures_tower = tf.keras.layers.GaussianDropout(0.4)(movie_fixFeatures_tower)
        ## user fix features tower
        user_fixFeatures_tower = tf.keras.layers.DenseFeatures(self.user_fixFeatures_columns,
                                                               name='user_fixFeatures_tower_embedding')(
            self.user_fixFeatures_input)
        for i, num_nodes in enumerate(hidden_units):
            user_fixFeatures_tower = tf.keras.layers.Dense(num_nodes, activation='relu',
                                                           name='user_fixFeatures_tower_MLP' + str(i))(
                user_fixFeatures_tower)
        user_fixFeatures_tower = tf.keras.layers.GaussianDropout(0.4)(user_fixFeatures_tower)
        # fix features interact
        fix_interact = tf.keras.layers.Dot(axes=1, name='fix_interact')(
            [movie_fixFeatures_tower, user_fixFeatures_tower])
        # dynamics tower
        if not self.basicModel:
            ## movie dynamic features tower
            movie_dynamicFeatures_tower = tf.keras.layers.DenseFeatures(self.movie_dynamicFeatures_columns,
                                                                        name='movie_dynamicFeatures_tower_embedding')(
                self.movie_dynamicFeatures_input)
            for i, num_nodes in enumerate(hidden_units):
                movie_dynamicFeatures_tower = tf.keras.layers.Dense(num_nodes, activation='relu',
                                                                    name='movie_dynamicFeatures_tower_MLP' + str(i))(
                    movie_dynamicFeatures_tower)
            movie_dynamicFeatures_tower = tf.keras.layers.GaussianDropout(0.4)(movie_dynamicFeatures_tower)

            ## user dynamic features tower
            user_dynamicFeatures_tower = tf.keras.layers.DenseFeatures(self.user_dynamicFeatures_columns,
                                                                       name='user_dynamicFeatures_tower_embedding')(
                self.user_dynamicFeatures_input)
            for i, num_nodes in enumerate(hidden_units):
                user_dynamicFeatures_tower = tf.keras.layers.Dense(num_nodes, activation='relu',
                                                                   name='user_dynamicFeatures_tower_MLP' + str(i))(
                    user_dynamicFeatures_tower)
            user_dynamicFeatures_tower = tf.keras.layers.GaussianDropout(0.4)(user_dynamicFeatures_tower)

            ## dymamic interact
            dynamic_interact = tf.keras.layers.Dot(axes=1, name='dynamic_interact')(
                [movie_dynamicFeatures_tower, user_dynamicFeatures_tower])

            # movie-user interactive layer
            sim_layer = tf.keras.layers.Dot(axes=1, name='movie_user_interact')([dynamic_interact, fix_interact])
            output = tf.keras.layers.Dense(1, activation='relu')(sim_layer)
            self.model = tf.keras.Model(self.feature_inputs, output, name='TwoTowerNeuralCF-DynamicFeatures')

        else:
            sim_layer = fix_interact
            output = tf.keras.layers.Dense(1, activation='relu')(sim_layer)
            if not self.onlyUseId:
                self.model = tf.keras.Model(self.feature_inputs, output, name='TwoTowerNeuralCF-FixedFeatures')
            else:
                self.model = tf.keras.Model(self.feature_inputs, output, name='basicTwoTowerNeuralCF')
        self.model.compile(
            loss='mse',
            optimizer=tf.keras.optimizers.Adam(lr=1e-3),
            metrics=[tf.keras.metrics.RootMeanSquaredError(), 'mae'])
        self.model.build(self.batch_size)
        print(self.model.name)
        model_arch_filePath = self.fname + '-' + self.model.name + '.png'
        plot_model(self.model, to_file=model_arch_filePath, dpi=300)

        return self.model

    def modelSummary(self):
        self.model.summary(line_length=150)

    def modelFit(self, epochs):

        # train the model
        # tensorboard callback for visualization
        self.log_dir = "logs/fit/" + self.fname + "/" + self.model.name + "/" + CURRENT_TIME + '/'
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=self.log_dir, histogram_freq=1, write_images=True)

        # ModelCheckpoint to save the best model
        # checkpoint_path = './checkpoint/' + self.fname + "-" + self.model.name + "-" + currentTime + '.best.ckpt'
        self.checkpoint_path = './checkpoint/' + self.fname + "/" + self.model.name + "/" + CURRENT_TIME + '/'
        model_checkPoint_callback = tf.keras.callbacks.ModelCheckpoint(filepath=self.checkpoint_path,
                                                                       monitor='val_root_mean_squared_error',
                                                                       save_best_only=True, save_weights_only=False,
                                                                       verbose=1)
        earlyStopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=10)

        self.callbacks_lists = [tensorboard_callback, model_checkPoint_callback, earlyStopping]
        print('Start to fit {} model on dataset {} during {} epochs'.format(self.model.name, self.fname, epochs))
        start = time.time()
        self.History = self.model.fit(self.train,
                                      validation_data=self.test,
                                      epochs=epochs,
                                      callbacks=self.callbacks_lists, verbose=2)
        end = time.time()
        print('Complete fiting {} model on dataset {}, spend a total of {} seconds'.format(self.model.name, self.fname,
                                                                                           end - start))
        with open('./histories/' + self.fname + '/' + self.model.name + '.pkl', 'wb') as f:
            pickle.dump(self.History.history, f)

    def modelEvalutae(self):

        # evaluate the model
        test_loss, test_rmse, test_mae = self.model.evaluate(self.test)
        print('\n\nTest Loss {}, Test rmse {}, Test mae {}'.format(test_loss, test_rmse, test_mae))

        # print some predict results
        predictions = self.model.predict(self.test)
        for prediction, real in zip(predictions[:10], list(self.test)[0][1][:10]):
            print("Prediction Rating Score: {:.2}".format(prediction[0]),
                  " | Actual rating label: ", float(real))


class neuralCF_moreFea(twoTower_neuralCF_moreFea):

    def build_model(self, hidden_units: list):
        # fix feature tower
        ## movie fix features tower
        movie_fixFeatures_tower = tf.keras.layers.DenseFeatures(self.movie_fixFeatures_columns,
                                                                name='movie_fixFeatures_tower_embedding')(
            self.movie_fixFeatures_input)
        if not self.onlyUseId:
            movie_fixFeatures_tower = tf.keras.layers.Dense(128)(movie_fixFeatures_tower)
        movie_fixFeatures_tower = tf.keras.layers.GaussianDropout(0.4)(movie_fixFeatures_tower)

        ## user fix features tower
        user_fixFeatures_tower = tf.keras.layers.DenseFeatures(self.user_fixFeatures_columns,
                                                               name='user_fixFeatures_tower_embedding')(
            self.user_fixFeatures_input)
        if not self.onlyUseId:
            user_fixFeatures_tower = tf.keras.layers.Dense(128)(user_fixFeatures_tower)
        user_fixFeatures_tower = tf.keras.layers.GaussianDropout(0.4)(user_fixFeatures_tower)
        # fix features interact
        fix_interact = tf.keras.layers.Dot(axes=1, name='fix_interact')(
            [movie_fixFeatures_tower, user_fixFeatures_tower])

        for i, num_nodes in enumerate(hidden_units):
            fix_interact = tf.keras.layers.Dense(num_nodes, activation='relu',
                                                 name='fix_interact_MLP' + str(i))(
                fix_interact)
        fix_interact_dropout_layer = tf.keras.layers.GaussianDropout(0.4)(fix_interact)

        # dynamics tower
        if not self.basicModel:
            ## movie dynamic features tower
            movie_dynamicFeatures_tower = tf.keras.layers.DenseFeatures(self.movie_dynamicFeatures_columns,
                                                                        name='movie_dynamicFeatures_tower_embedding')(
                self.movie_dynamicFeatures_input)
            movie_dynamicFeatures_tower = tf.keras.layers.Dense(128)(movie_dynamicFeatures_tower)
            movie_dynamicFeatures_tower = tf.keras.layers.GaussianDropout(0.4)(movie_dynamicFeatures_tower)

            ## user dynamic features tower
            user_dynamicFeatures_tower = tf.keras.layers.DenseFeatures(self.user_dynamicFeatures_columns,
                                                                       name='user_dynamicFeatures_tower_embedding')(
                self.user_dynamicFeatures_input)
            user_dynamicFeatures_tower = tf.keras.layers.Dense(128)(user_dynamicFeatures_tower)
            user_dynamicFeatures_tower = tf.keras.layers.GaussianDropout(0.4)(user_dynamicFeatures_tower)

            ## dymamic interact
            dynamic_interact = tf.keras.layers.Dot(axes=1, name='dynamic_interact')(
                [movie_dynamicFeatures_tower, user_dynamicFeatures_tower])
            for i, num_nodes in enumerate(hidden_units):
                dynamic_interact = tf.keras.layers.Dense(num_nodes, activation='relu',
                                                         name='dynamic_interact_MLP' + str(i))(
                    dynamic_interact)
            dynamic_interact_dropout_layer = tf.keras.layers.GaussianDropout(0.4)(dynamic_interact)

            # movie-user interactive
            sim_layer = tf.keras.layers.Dot(axes=1, name='movie_user_interact')(
                [dynamic_interact_dropout_layer, fix_interact_dropout_layer])
            output = tf.keras.layers.Dense(1, activation='relu')(sim_layer)
            self.model = tf.keras.Model(self.feature_inputs, output, name='NeuralCF-DynamicFeatures')

        else:
            sim_layer = fix_interact_dropout_layer
            output = tf.keras.layers.Dense(1, activation='relu')(sim_layer)
            if not self.onlyUseId:
                self.model = tf.keras.Model(self.feature_inputs, output, name='NeuralCF-FixedFeatures')
            else:
                self.model = tf.keras.Model(self.feature_inputs, output, name='basicNeuralCF')
        self.model.compile(
            loss='mse',
            optimizer=tf.keras.optimizers.Adam(lr=1e-3),
            metrics=[tf.keras.metrics.RootMeanSquaredError(), 'mae'])
        self.model.build(self.batch_size)
        model_arch_filePath = self.fname + '-' + self.model.name + '.png'
        plot_model(self.model, to_file=model_arch_filePath, dpi=300)
        print('Architecture of {} model is saved as {}'.format(self.model.name, model_arch_filePath))


def run(fname, epochs=10, useNeuralCF=0, onlyUseId=1, useBasicModel=1, onlySummary=1):
    """

    Args:
        fname: dataset name ['ml-1m', 'ml-100k']
        epochs: int
        useNeuralCF: use NeuralCF or Two Tower NeuralCF
        onlyUseId: flag 0 or 1
        useBasicModel: 0 or 1
        onlySummary: 0 or 1
        save: save model by using pickle
    Returns:

    """
    if useNeuralCF:
        model = neuralCF_moreFea(fname, useBasicModel, onlyUseId)
    else:
        model = twoTower_neuralCF_moreFea(fname, useBasicModel, onlyUseId)
    model.generate_input_featureColumns()
    emptyModel = model.build_model([128])
    if onlySummary:
        emptyModel.summary(line_length=150)
        return
    model.modelFit(epochs)
    model.modelEvalutae()


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--fname', type=str, choices=['ml-1m', 'ml-100k'], help='choose the dataset', default='ml-100k')
    parser.add_argument('--onlyUseId', type=int, choices=[0, 1], help='onlyUseId or not', default=0)
    parser.add_argument('--useBasicModel', type=int, choices=[0, 1], help='useBasicModel or not', default=1)
    parser.add_argument('--epochs', type=int, help='set the epochs for your model', default=100)
    parser.add_argument('--useNeuralCF', type=int, help='choose model', default=0)
    parser.add_argument('--onlySummary', type=int, choices=[0, 1], help='onlySummary or not', default=0)
    parser.add_argument('--run', action='store_true', help='start training model')
    args = parser.parse_args()
    if args.onlyUseId and not args.useBasicModel:
        raise ValueError('onlyUseId only available for BasicModle')

    fname = args.fname
    epochs = args.epochs
    useNeuralCF = args.useNeuralCF
    onlyUseId = args.onlyUseId
    useBasicModel = args.useBasicModel  # 1 Two Tower Neural BasicModel: fixFeature, else 0 dynamic feature
    onlySummary = args.onlySummary

    if args.run:
        run(fname, epochs, useNeuralCF, onlyUseId, useBasicModel, onlySummary)
    else:
        parser.print_help()
